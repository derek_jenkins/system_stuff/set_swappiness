#!/bin/bash
# # # # #
# Return codes
E_BAD_ARGS="35" E_NOT_ROOT="45" E_BAD_CHOICE="55" E_MULTIPLE_CONFIGS="65" E_UNEXPECTED_CONFIGS="75"
# # # # #
# File related vars
SYSCTL_99_CONF="/etc/sysctl.d/99-sysctl.conf" SYSCTL_DEFAULT_CONF="/etc/sysctl.conf"
# # # # #
# Funcs
must_be_ran_as_root() {
    [[ "${UID}" = 0 ]] || {
        echo "This script must be ran as the root user. Exiting now."
        exit "${E_NOT_ROOT}"
    }
}
bad_choice() {
    echo -e "\nYour choice \"${SWAPPINESS_CHOICE}\" is not an accepted value. Please choose a number between 0 and 100."
    exit "${E_BAD_CHOICE}"
}
bad_args() {
    echo "This script can either be ran interactively or can be given one numerical argument between 0 and 100 inclusive."
    exit "${E_BAD_ARGS}"
}
find_swappiness_config() {
    grep -rl ^vm.swappiness /etc/sysctl.d/*.conf
}
show_swappiness_config() {
    echo -e "\nWe found a vm.swappiness entry and will make our edit here:\n\n${1}"
}
parse_args_then_edit_swappiness() {
    if [[ "$#" -eq 1 ]]; then
        SWAPPINESS_CHOICE="${1}"
        heavy_lifting
    elif [[ "$#" -eq 0 ]]; then
        echo -e "Current swappiness level: $(sysctl vm.swappiness)\n"
        read -p "Please choose your new swappiness level here. Pick a number from 0 to 100: " SWAPPINESS_CHOICE
        heavy_lifting
    else
        bad_args
    fi
}
lint_user_input() {
    case "${SWAPPINESS_CHOICE}" in
        ''|*[!0-9]*)
            bad_choice
        ;;
        *)
        if [[ ${SWAPPINESS_CHOICE} -ge 0 && ${SWAPPINESS_CHOICE} -le 100 ]]; then
            return
        else
            bad_choice
        fi
        ;;
    esac
}
heavy_lifting() {
    lint_user_input
    if [[ $(find_swappiness_config | wc -l) = 1 && $(find_swappiness_config) = "${SYSCTL_99_CONF}" ]]; then
        if [[ $(grep ^vm.swappiness "${SYSCTL_DEFAULT_CONF}") ]]; then
            show_swappiness_config "${SYSCTL_DEFAULT_CONF}"
            SWAPPINESS_CONFIG="${SYSCTL_DEFAULT_CONF}"
            make_the_swappiness_edit
        else
            echo -e "\nIt appears that you have vm.swappiness set in ${SYSCTL_99_CONF}, but we aren't seeing the same value in ${SYSCTL_DEFAULT_CONF}.\n\nThis script is now exiting so you can make the required edits manually."
            exit "${E_UNEXPECTED_CONFIGS}"
        fi
    elif [[ $(find_swappiness_config | wc -l) = 1 && $(find_swappiness_config) != ${SYSCTL_99_CONF} ]]; then
        show_swappiness_config "$(find_swappiness_config)"
        SWAPPINESS_CONFIG=$(find_swappiness_config)
        make_the_swappiness_edit
    elif [[ $(find_swappiness_config | wc -l ) = 0 ]]; then
        echo -e "\nWe didn't find any config under /etc that specs vm.swappiness, so we will simply echo this entry into the default config at ${SYSCTL_DEFAULT_CONF}."
        SWAPPINESS_CONFIG="${SYSCTL_DEFAULT_CONF}"
        make_the_swappiness_edit
    elif [[ $(find_swappiness_config | wc -l) -gt 1 ]]; then
        echo -e "\nIt appears that there is more than one valid configuration file in /etc/sysctl.d that contains a vm.swappiness reference:\n\n$(find_swappiness_config)\n\nBecause of this, this script is now exiting. Please set your swappiness level manually."
        exit "${E_MULTIPLE_CONFIGS}"
    fi
}
make_the_swappiness_edit() {
    sed -i.bak."$(date +%s)" '/vm.swappiness=*/d' "${SWAPPINESS_CONFIG}"
    echo "vm.swappiness=${SWAPPINESS_CHOICE}" >> "${SWAPPINESS_CONFIG}"
    sysctl -qp "${SWAPPINESS_CONFIG}"
    echo -e "\nsysctl has been reloaded and your new swappiness level is:\n$(sysctl vm.swappiness)"
}
# # # # #
# "Main"
must_be_ran_as_root
parse_args_then_edit_swappiness "$@"
exit "$?"